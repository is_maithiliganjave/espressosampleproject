package org.gnucash.android.test.ui;

import android.content.Intent;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.rule.ActivityTestRule;
import org.gnucash.android.R;
import org.gnucash.android.test.ui.util.Utilities;
import org.gnucash.android.ui.account.AccountsActivity;
import org.junit.Rule;
import org.junit.Test;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.clearText;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayingAtLeast;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

public class TestAccountActivity {
    private static final String SAMPLE_ACCOUNT_NAME="AccountTest";
    private static final String SAMPLE_SUB_ACCOUNT_NAME="SubAccountTest";
    private static final String EDIT_ACCOUNT_NAME = "EditedAccount";
    private static final String SAMPLE_TRANSACTION_NAME="TestTransaction";
    private static final String BOOK_NAME="Book 5";
    private static final String USE_ACCOUNT="Expenses";
    private static final String TRANSACTION_AMOUNT="200.00";
    private static final String RENAME_BOOK="Update Book";
    private static final String SUB_ACCOUNT="Adjustment";

    @Rule
    public ActivityTestRule<AccountsActivity> mActivityRule = new ActivityTestRule<>(AccountsActivity.class);

    @Test
    public void testInputAfterRelaunch()  {
        onView(withId(R.id.fab_create_account)).perform(click());
        onView(withId(R.id.input_account_name)).perform(typeText(SAMPLE_ACCOUNT_NAME));
        mActivityRule.finishActivity();
        mActivityRule.launchActivity(new Intent());
        onView(withId(R.id.fab_create_account)).perform(click());
        onView(withId(R.id.input_account_name)).check(matches(withText("")));
    }

    @Test
    public void testCreateAccount() {
        onView(withId(R.id.fab_create_account)).perform(click());
        onView(withId(R.id.input_account_name)).perform(typeText(SAMPLE_ACCOUNT_NAME));
        onView(withId(R.id.menu_save)).perform(click());
        onView(withId(R.id.tab_layout)).perform(swipeLeft(),click());
        ViewInteraction account = onView(withText(SAMPLE_ACCOUNT_NAME));
        account.check(matches(isDisplayed()));
    }

    @Test
    public void testEditAccountDetails()
    {
        onView(withId(R.id.fab_create_account)).perform(click());
        onView(withId(R.id.input_account_name)).perform(typeText(SAMPLE_ACCOUNT_NAME));
        onView(withId(R.id.menu_save)).perform(click());
        onView(withId(R.id.tab_layout)).perform(swipeLeft(),click());
        onView(allOf(withParent(hasDescendant(withText(SAMPLE_ACCOUNT_NAME))),
                withId(R.id.options_menu))).perform(click());
        onView(withText(R.string.title_edit_account)).check(matches(isDisplayed())).perform(click());
        onView(withId(R.id.fragment_account_form)).check(matches(isDisplayed()));
        onView(withId(R.id.input_account_name)).perform(clearText()).perform(typeText(EDIT_ACCOUNT_NAME));
        onView(withId(R.id.menu_save)).perform(click());
        onView(withText(SAMPLE_ACCOUNT_NAME)).check(doesNotExist());
        onView(withText(EDIT_ACCOUNT_NAME)).check(matches(isDisplayed()));
    }

    @Test
    public void testDeleteAccount(){
        onView(allOf(withParent(hasDescendant(withText(SAMPLE_ACCOUNT_NAME))),
                withId(R.id.options_menu))).perform(click());
        onView(withText(R.string.menu_delete)).perform(scrollTo(),click());
        onView(withText(EDIT_ACCOUNT_NAME)).check(doesNotExist());
    }

    @Test
    public void testTransaction(){
        Utilities.createAccountWithName(SAMPLE_ACCOUNT_NAME);
        onView(withText(SAMPLE_ACCOUNT_NAME)).perform(click());
        onView(withId(R.id.fab_create_transaction)).perform(click());
        onView(withId(R.id.input_transaction_name)).perform(typeText(SAMPLE_TRANSACTION_NAME));
        onView(withId(R.id.input_transaction_amount)).perform(typeText("2.00"),closeSoftKeyboard());
        onView(withId(R.id.input_transaction_type)).perform(click());
        onView(withId(R.id.menu_save)).perform(click());
        onView(withText(SAMPLE_TRANSACTION_NAME)).check(matches(isDisplayed()));
        onView(withId(R.id.transaction_amount)).check(matches(withText("$2.00")));
       }

       @Test
       public void testBooks(){
           onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
           onView(withId(R.id.book_name)).check(matches(isDisplayed())).perform(click());
           onView(withText(R.string.menu_manage_books)).perform(click());
           onView(withId(R.id.menu_create_book)).perform(click());
            Utilities.sleep();
           onView(allOf(
                   withId(R.id.account_recycler_view))).check(matches(isDisplayingAtLeast(5)));
       }

    @Test
    public void testCreateSubAccount() {
        onView(withText(USE_ACCOUNT))
                .perform(click());
        onView(withId(R.id.fab_create_transaction)).perform(click());
        onView(withId(R.id.input_account_name)).perform(typeText(SAMPLE_SUB_ACCOUNT_NAME));
        onView(withId(R.id.menu_save)).perform(click());
        onView(withText(SAMPLE_SUB_ACCOUNT_NAME)).check(matches(isDisplayed()));
    }

    @Test
    public void testEditBook(){
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.book_name)).check(matches(isDisplayed())).perform(click());
        onView(withText(R.string.menu_manage_books)).perform(click());
        onView(allOf(withParent(hasDescendant(withText(BOOK_NAME))),
                withId(R.id.options_menu))).perform(click());
        onView(withText("Rename")).perform(click());
        onView(withId(R.id.input_book_title)).perform(clearText(),typeText(RENAME_BOOK));
        onView(withText("Rename")).perform(click());
        onView(withText(RENAME_BOOK)).check(matches(isDisplayed()));
        onView(withId(R.id.menu_create_book)).perform(click());
    }

    @Test
    public void testDeleteBook(){
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.book_name)).check(matches(isDisplayed())).perform(click());
        onView(withText(R.string.menu_manage_books)).perform(click());
        onView(allOf(withParent(hasDescendant(withText(RENAME_BOOK))),
                withId(R.id.options_menu))).perform(click());
        onView(allOf(withId(R.id.title),withText("Delete"))).perform(click());
        onView(withText(RENAME_BOOK)).check(doesNotExist());
    }

    @Test
    public void testPieChartReportWithTransaction()
    {
        onView(withText(USE_ACCOUNT))
                .perform(click());
        onView(withText("Auto"))
                .perform(click());
        onView(withText("Fees"))
                .perform(click());
        onView(withId(R.id.fab_create_transaction)).perform(click());
        onView(withId(R.id.input_transaction_name)).perform(typeText(SAMPLE_TRANSACTION_NAME));
        onView(withId(R.id.input_transaction_amount)).perform(typeText(TRANSACTION_AMOUNT),closeSoftKeyboard());
        onView(withId(R.id.input_transaction_type)).perform(click());
        onView(withId(R.id.menu_save)).perform(click());
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(allOf(withId(R.id.design_menu_item_text),withText("Reports"))).perform(click());
        onView(withId(R.id.total_assets)).check(matches(withText("$"+TRANSACTION_AMOUNT)));
        Espresso.pressBack();
        onView(allOf(withParent(hasDescendant(withText(SAMPLE_TRANSACTION_NAME))),
                withId(R.id.options_menu))).perform(click());
        onView(withText("Delete")).perform(scrollTo(),click());
    }


    @Test
    public void testCreateBackup() {
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.nav_view)).perform(swipeUp());
        onView(withText("Settings")).perform(click());
        onView(withText("Backup & export")).perform(click());
        onView(withText("Create Backup")).perform(click());
        onView(withText("Backup successful"))
                .check(matches(isDisplayed()));
    }

    @Test
    public void verifyFouritesList(){
        onView(withText(USE_ACCOUNT))
                .perform(click());
        onView(allOf(withParent(hasDescendant(withText("Auto"))),
                withId(R.id.favorite_status))).perform(click());
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(allOf(withId(R.id.design_menu_item_text),withText("Favorites"))).perform(click());
        onView(withText("Auto")).check(matches(isDisplayed()));
        onView(allOf(withParent(hasDescendant(withText("Auto"))),
                withId(R.id.favorite_status))).perform(click());
        onView(withText("Auto")).check(doesNotExist());
        onView(withId(R.id.tab_layout)).perform(swipeRight(),click());
    }

    @Test
    public void verifyDuplicateTransactions()
    {
        onView(withText(USE_ACCOUNT))
                .perform(click());
        onView(withText(SUB_ACCOUNT))
                .perform(click());
        onView(withId(R.id.fab_create_transaction)).perform(click());
        onView(withId(R.id.input_transaction_name)).perform(typeText(SAMPLE_TRANSACTION_NAME));
        onView(withId(R.id.input_transaction_amount)).perform(typeText(TRANSACTION_AMOUNT),closeSoftKeyboard());
        onView(withId(R.id.input_transaction_type)).perform(click());
        onView(withId(R.id.menu_save)).perform(click());
        onView(allOf(
                withId(R.id.transaction_recycler_view))).check(matches(not(isDisplayingAtLeast(2))));
        onView(allOf(withParent(hasDescendant(withText(SAMPLE_TRANSACTION_NAME))),
                withId(R.id.options_menu))).perform(click());
        onView(withText("Duplicate")).perform(scrollTo(),click());
        onView(allOf(
                withId(R.id.transaction_recycler_view))).check(matches(isDisplayingAtLeast(2)));
    }

    @Test
    public void createNewBook()
    {
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.drawer_layout)).perform(swipeUp());
        onView(withText("Settings")).perform(click());
        onView(withText("Accounts")).perform(click());
        onView(withText("Create default accounts")).perform(click());
        onView(withId(android.R.id.button1)).perform(click());
        Utilities.sleep();
        onView(allOf(
                withId(R.id.account_recycler_view))).check(matches(isDisplayingAtLeast(5)));
    }

    @Test
    public void testSearchAccount()
    {
        String SEARCH_ACCOUNT_NAME = "Cable";
        onView(withId(R.id.menu_search)).perform(click());
        onView(withId(R.id.search_src_text)).perform(typeText("Cab"));
        onView(withText(SEARCH_ACCOUNT_NAME)).check(matches(isDisplayed()));
        onView(withId(R.id.search_src_text)).perform(clearText());
        onView(withText(SEARCH_ACCOUNT_NAME)).check(doesNotExist());
    }

    @Test
    public void testSplitAmount()
    {
       onView(withText(USE_ACCOUNT))
                .perform(click());
        onView(withText(SUB_ACCOUNT))
                .perform(click());
        onView(withId(R.id.fab_create_transaction)).perform(click());
        onView(withId(R.id.input_transaction_name)).perform(typeText(SAMPLE_TRANSACTION_NAME));
        onView(withId(R.id.input_transaction_amount)).perform(typeText(TRANSACTION_AMOUNT),closeSoftKeyboard());
        onView(withId(R.id.btn_split_editor)).perform(click());
        onView(withId(R.id.split_list_layout)).check(matches(allOf(isDisplayed(), hasDescendant(withId(R.id.input_split_amount)))));
        onView(withId(R.id.menu_save)).perform(click());
        onView(withId(R.id.menu_save)).perform(click());
        onView(allOf(withParent(hasDescendant(withText(SAMPLE_TRANSACTION_NAME))),
                withId(R.id.options_menu))).perform(click());
        onView(withText("Delete")).perform(scrollTo(),click());
    }

    @Test
    public void createSubAccountWithAnotherType()
    {
        onView(withText(USE_ACCOUNT))
                .perform(click());
        onView(withText(SUB_ACCOUNT))
                .perform(click());
        onView(withId(R.id.tab_layout)).perform(swipeRight(),click());
        onView(withId(R.id.fab_create_transaction)).perform(click());
        onView(withId(R.id.input_account_name)).perform(typeText(SAMPLE_SUB_ACCOUNT_NAME));
        onView(allOf(withId(android.R.id.text1),withText("EXPENSE"))).perform(click());
        onView(allOf(withId(android.R.id.text1),withText("EQUITY"))).perform(click());
        onView(withId(R.id.menu_save)).perform(click());
        onView(withText(SAMPLE_SUB_ACCOUNT_NAME)).check(doesNotExist());
        Espresso.pressBack();
        Espresso.pressBack();
        onView(withText("Equity")).perform(click());
        onView(withText(SAMPLE_SUB_ACCOUNT_NAME)).check(matches(isDisplayed()));
        onView(allOf(withParent(hasDescendant(withText(SAMPLE_SUB_ACCOUNT_NAME))),
                withId(R.id.options_menu))).perform(click());
        onView(withText("Delete")).perform(scrollTo(),click());
    }

    @Test
    public void testDisplayingSubAccounts()
    {
        onView(withText("Assets"))
                .perform(click());
        onView(withId(R.id.toolbar_spinner)).perform(click());
        onView(allOf(withId(android.R.id.text1),withText("Expenses"))).perform(click());
        onView(withId(android.R.id.text1)).check(matches(withText("Expenses")));
        onView(allOf(
                withId(R.id.account_recycler_view))).check(matches(isDisplayingAtLeast(25)));
    }

    @Test
    public void testSchedulerAction()
    {
        onView(withText(USE_ACCOUNT))
                .perform(click());
        onView(withText(SUB_ACCOUNT))
                .perform(click());
        onView(withId(R.id.fab_create_transaction)).perform(click());
        onView(withId(R.id.input_transaction_name)).perform(typeText(SAMPLE_TRANSACTION_NAME));
        onView(withId(R.id.input_transaction_amount)).perform(typeText(TRANSACTION_AMOUNT),closeSoftKeyboard());
        onView(withId(R.id.input_recurrence)).perform(click());
        onView(withId(R.id.repeat_switch)).perform(click());
        onView(withId(R.id.done_button)).perform(click());
        onView(withId(R.id.menu_save)).perform(click());
        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.nav_view)).perform(swipeUp());
        onView(withText("Scheduled Actions")).perform(click());
        onView(withText(SAMPLE_TRANSACTION_NAME)).check(matches(isDisplayed()));
        onView(withId(R.id.checkbox)).perform(click());
        onView(withId(R.id.context_menu_delete)).perform(click());
    }

    @Test
    public void testCurrencyForAccountTransactions(){
        onView(withText(USE_ACCOUNT))
                .perform(click());
        onView(withText(SUB_ACCOUNT))
                .perform(click());
        onView(withId(R.id.tab_layout)).perform(swipeRight(),click());
        onView(withId(R.id.fab_create_transaction)).perform(click());
        onView(withId(R.id.input_account_name)).perform(typeText(SAMPLE_SUB_ACCOUNT_NAME));
        onView(withId(R.id.input_currency_spinner)).perform(click());
        onView(allOf(withId(android.R.id.text1),withText("USD - US Dollar"))).perform(click());
        onView(withId(R.id.menu_save)).perform(click());
        onView(withText(SAMPLE_SUB_ACCOUNT_NAME)).perform(click());
        onView(withId(R.id.fab_create_transaction)).perform(click());
        onView(withId(R.id.currency_symbol)).check(matches(withText("$")));
    }

}