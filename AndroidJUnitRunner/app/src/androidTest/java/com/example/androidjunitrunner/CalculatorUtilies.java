package com.example.androidjunitrunner;

import androidx.test.espresso.ViewInteraction;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

public class CalculatorUtilies {

    public Double getDoubleFromString(String operand)
    {
        return Double.parseDouble(operand);
    }

    public String getStringFromDouble(Double result)
    {
        return Double.toString(result);
    }

    public String getResult(String operation,String operandOne, String operandTwo)
    {
        String result="";
        switch(operation) {
            case "Addition" : result=getStringFromDouble(getDoubleFromString(operandOne)+getDoubleFromString(operandTwo));
                                break;
            case "Subtraction" : result=getStringFromDouble(getDoubleFromString(operandOne)-getDoubleFromString(operandTwo));
                                break;
            case "Multiplication" : result=getStringFromDouble(getDoubleFromString(operandOne)*getDoubleFromString(operandTwo));
                                break;
            case "Division" :if(operandTwo=="0"){
                                result="Error";
                                break;
                             }
                             else {
                                result = getStringFromDouble(getDoubleFromString(operandOne) / getDoubleFromString(operandTwo));
                                break;
                             }
        }

        return result;
    }

    public void provideOperands(String operandOne,String operandTwo)
    {
        ViewInteraction textBoxOperandOne = onView(withId(R.id.operand_one_edit_text));
        textBoxOperandOne.perform(typeText(operandOne),closeSoftKeyboard());
        ViewInteraction textBoxOperandTwo = onView(withId(R.id.operand_two_edit_text));
        textBoxOperandTwo.perform(typeText(operandTwo),closeSoftKeyboard());
    }
}
