package com.example.androidjunitrunner;

import androidx.test.espresso.ViewInteraction;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
public class BasicCalculatorTest {
    CalculatorUtilies calculatorUtilies;
    String operandOne;
    String operandTwo;

    @Rule
    public ActivityScenarioRule<CalculatorActivity> activityTestRule = new ActivityScenarioRule<CalculatorActivity>(CalculatorActivity.class);

    @Before
    public void beforeTest()
    {
        operandOne="10";
        operandTwo="20";
        calculatorUtilies=new CalculatorUtilies();
    }

    @Test
    public void verifyAddition()
    {
        calculatorUtilies.provideOperands(operandOne,operandTwo);
        onView(withId(R.id.operation_add_btn)).perform(click());
        onView(withId(R.id.operation_result_text_view)).check(matches(isDisplayed()));
        onView(withId(R.id.operation_result_text_view)).check(matches(withText(calculatorUtilies.getResult("Addition",operandOne,operandTwo))));
    }

    @Test
    public void verifySubtraction()
    {
        calculatorUtilies.provideOperands(operandOne,operandTwo);
        onView(withId(R.id.operation_sub_btn)).perform(click());
        onView(withId(R.id.operation_result_text_view)).check(matches(isDisplayed()));
        onView(withId(R.id.operation_result_text_view)).check(matches(withText(calculatorUtilies.getResult("Subtraction",operandOne,operandTwo))));
    }

    @Test
    public void verifyMultiplication()
    {
        calculatorUtilies.provideOperands(operandOne,operandTwo);
        onView(withId(R.id.operation_mul_btn)).perform(click());
        onView(withId(R.id.operation_result_text_view)).check(matches(isDisplayed()));
        onView(withId(R.id.operation_result_text_view)).check(matches(withText(calculatorUtilies.getResult("Multiplication",operandOne,operandTwo))));
    }

   @Test
    public void verifyDivision()
    {
        calculatorUtilies.provideOperands(operandOne,operandTwo);
        onView(withId(R.id.operation_div_btn)).perform(click());
        onView(withId(R.id.operation_result_text_view)).check(matches(isDisplayed()));
        onView(withId(R.id.operation_result_text_view)).check(matches(withText(calculatorUtilies.getResult("Division",operandOne,operandTwo))));
    }

   @Test
    public void verifyDivisionByZero()
    {
        calculatorUtilies.provideOperands(operandOne,"0");
        onView(withId(R.id.operation_div_btn)).perform(click());
        onView(withId(R.id.operation_result_text_view)).check(matches(isDisplayed()));
        onView(withId(R.id.operation_result_text_view)).check(matches(withText(calculatorUtilies.getResult("Division",operandOne,"0"))));
    }
}
