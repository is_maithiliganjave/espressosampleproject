package com.example.myapplication;

import android.content.Context;
import android.content.Intent;

import androidx.test.uiautomator.By;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiObjectNotFoundException;
import androidx.test.uiautomator.UiSelector;
import androidx.test.uiautomator.Until;


import org.junit.Before;
import org.junit.Test;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;
import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertTrue;

public class FirstTestUiAutomator{
    private static final String BASIC_SAMPLE_PACKAGE
            = "com.example.myapplication";

    private static final int LAUNCH_TIMEOUT = 5000;

    private UiDevice mDevice;
    @Before
    public void sampleUiAutomator() {
        mDevice = UiDevice.getInstance(getInstrumentation());
       Context context = getApplicationContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(BASIC_SAMPLE_PACKAGE);
       context.startActivity(intent);

        // Wait for the app to appear
       mDevice.wait(Until.hasObject(By.pkg(BASIC_SAMPLE_PACKAGE).depth(0)), LAUNCH_TIMEOUT);

    }

    @Test
    public void verifyApp() throws UiObjectNotFoundException {
       UiObject next = mDevice.findObject(new UiSelector().text("NEXT"));
        next.clickAndWaitForNewWindow(3000);
        UiObject previous = mDevice.findObject(new UiSelector().text("PREVIOUS"));
        assertTrue(previous.exists());
    }
}