package com.example.recyclerviewlistassignment;

import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.core.app.ApplicationProvider.getApplicationContext;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class RecyclerViewTest {
    @Rule
    public ActivityScenarioRule<MainActivity> activityTestRule = new ActivityScenarioRule<MainActivity>(MainActivity.class);

    @Test
    public void testFirstRecyclerView() throws InterruptedException {
       // First, scroll to the position that needs to be matched and click on it.
        onView(ViewMatchers.withId(R.id.recyclerView))
                .perform(RecyclerViewActions.actionOnItemAtPosition(15,
                        click()));
        Thread.sleep(3000);
        // Match the text in an item below the fold and check that it's displayed.
        String itemElementText = getApplicationContext().getResources()
                .getString(R.string.item_element_text)
                + String.valueOf(15);
        onView(withText(itemElementText)).check(matches(isDisplayed()));
        onView(withText(itemElementText)).check(matches(withText("This is element #15")));
    }
}