package com.example.espressoframework;


import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.com.example.espressoframework.EspressoFramework.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class VerifyAdapterViewList {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void verifyAdapterViewList() {
        ViewInteraction toggleButton = onView(
                allOf(withId(R.id.rowToggleButton), withText("ON"),
                        withParent(allOf(withId(R.id.item_wrapper),
                                withParent(withId(R.id.list)))),
                        isDisplayed()));
        toggleButton.check(matches(isDisplayed()));

        ViewInteraction textView = onView(
                allOf(withId(R.id.rowContentTextView), withText("item: 2"),
                        withParent(allOf(withId(R.id.item_wrapper),
                                withParent(withId(R.id.list)))),
                        isDisplayed()));
        textView.check(matches(withText("item: 2")));

        ViewInteraction toggleButton2 = onView(
                allOf(withId(R.id.rowToggleButton), withText("ON"),
                        withParent(allOf(withId(R.id.item_wrapper),
                                withParent(withId(R.id.list)))),
                        isDisplayed()));
        toggleButton2.check(matches(isDisplayed()));
    }
}
